package main

import (
  "fmt"
  "time"
  "math/rand"
)

func main() {
  fmt.Println("[[rock-paper-scissors game]]")
  
  //initalizing random number generator
  rand.Seed(time.Now().UnixNano())  
  
  var gotNeededInput bool = false
  var choice string = ""
  for !gotNeededInput {
    fmt.Printf("Choose rock, paper or scissors: ")
    
    _, err := fmt.Scanf("%s", &choice)
    if err != nil {
      fmt.Println("error while reading your choice. Try again.")
      continue  
    }
    
    if (choice != "rock") &&
       (choice != "paper") &&
       (choice != "scissors") { 
      fmt.Printf("You shoud choose rock, paper or scissors, not %s. Try again.\n", choice)
      continue
    }
    
    gotNeededInput = true
  }
  
  var variants []string = []string{ 
    "rock",
    "paper",
    "scissors", }
  var AIChoice string = variants[rand.Intn(len(variants))]
  fmt.Printf("My choice: %s\n", AIChoice)
  
  var playerWin bool = false;
  if AIChoice == "rock" {
    playerWin = choice == "paper"
  } else if AIChoice == "paper" {
    playerWin = choice == "scissors"
  } else if AIChoice == "scissors" {
    playerWin = choice == "rock"
  }
  
  if AIChoice == choice {
    fmt.Println("We both win")
  } else if playerWin {
    fmt.Println("You win!")
  } else {
    fmt.Println("I win!")
  }
}
